const checklenght = (password) => {
  return password.lenght >= 8 && password <= 25
}
const checkAlplabet = (password) => {
  // const alphabets = 'abcdefghigklmnopqrstuvwxyz'
  // for (const ch of password) {
  //   if (alphabets.includes(ch.toLowerCase())) return true
  // }
  // return false
  return /[a-zA-Z]/.test(password)
}
const checkDigit = (password) => {
  return /[0-9]/.test(password)
}
const checkSymbol = (password) => {
  const Symbols = '[!"#$%&()*+,-./:;<=>?@[]^_`{|}~]'
  for (const ch of password) {
    if (Symbols.includes(ch.toLowerCase())) return true
  }
  return false
  // return /[!"#$%&()*+,-./:;<=>?@[]^_`{|}~]/.test(password)
}
const checkPassword = (password) => {
  return checklenght(password) && checkAlplabet(password) && checkSymbol(password)
}
module.exports = {
  checklenght,
  checkAlplabet,
  checkDigit,
  checkSymbol,
  checkPassword

}
