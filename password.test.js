const { checklenght, checkAlplabet, checkSymbol, checkPassword } = require('./password')
describe('Test password Lenght', () => {
  test('should 8 characters to be true', () => {
    expect(checklenght('12345678')).toBe(false)
  })
  test('should 7 characters to be false', () => {
    expect(checklenght('1234567')).toBe(false)
  })
  test('should 25 characters to be true', () => {
    expect(checklenght('1111111111111111111111111')).toBe(false)
  })
  test('should 26 characters to be false', () => {
    expect(checklenght('11111111111111111111111111')).toBe(false)
  })
})

describe('Test Alplabet', () => {
  test('should has alphabet m in password', () => {
    expect(checkAlplabet('m')).toBe(true)
  })
  test('should has alphabet A in password', () => {
    expect(checkAlplabet('A')).toBe(true)
  })
  test('should has not alphabet in password', () => {
    expect(checkAlplabet('1111')).toBe(false)
  })
})

describe('Test Digit', () => {
  test('should has digit in password', () => {
    expect(checkAlplabet('rewrwer2')).toBe(true)
  })
  test('should has not digit in password', () => {
    expect(checkAlplabet('11111111111111')).toBe(false)
  })
})

describe('Test Symbol', () => {
  test('should has symbol ! in password', () => {
    expect(checkSymbol('11#11')).toBe(true)
  })
  test('should has not symbol in password', () => {
    expect(checkSymbol('11111111111111')).toBe(false)
  })
})

describe('Test Password', () => {
  // test('should has password to be true', () => {
  //   expect(checkPassword('oy@123')).toBe(true)
  // })
  test('should has password to be fasle', () => {
    expect(checkPassword('boy@123')).toBe(false)
  })
  test('should has password Noom@123 to be fasle', () => {
    expect(checkPassword('Noom@123')).toBe(false)
  })
})
